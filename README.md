# Victor Santiago

## Getting started

Clone the repo, install all project's dependencies with `npm install`, and run `npm run serve`. When it's done building, the app is accessible from http://localhost:8080/.
